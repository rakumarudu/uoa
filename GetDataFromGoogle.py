
# coding: utf-8

# In[65]:

#https://chrisconlan.com/download-historical-stock-data-google-r-python/
import matplotlib.pyplot as plt
import datetime as dt
import pandas as pd
import io
import requests
import time
 
def google_stocks(symbol, startdate = (1, 1, 2005), enddate = None):
 
    startdate = str(startdate[0]) + '+' + str(startdate[1]) + '+' + str(startdate[2])
 
    if not enddate:
        enddate = time.strftime("%m+%d+%Y")
    else:
        enddate = str(enddate[0]) + '+' + str(enddate[1]) + '+' + str(enddate[2])
 
    stock_url = "http://www.google.com/finance/historical?q=" + symbol +                 "&startdate=" + startdate + "&enddate=" + enddate + "&output=csv"
 
    raw_response = requests.get(stock_url).content
 
    stock_data = pd.read_csv(io.StringIO(raw_response.decode('utf-8')))
 
    return stock_data


# In[68]:

stock = 'NATI'
end_date = dt.datetime.today().strftime("%m/%d/%Y") #(9, 4, 2017)
stockInfo = google_stocks(stock, startdate= (1,1,2016),enddate = end_date)
print("Latest Price of stock = ", stockInfo['Close'][0],"$")
#For plotting reverse array and plot
prices = stockInfo['Close'].values
plt.plot(prices[::-1])
plt.show()


# In[51]:

print(stockInfo['Close'][0])
print(stockInfo['Date'][0])


# In[ ]:



