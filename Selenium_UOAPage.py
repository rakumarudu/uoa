# coding: utf-8


#Import all the required packages
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by  import By
from selenium.webdriver.support.select import Select
from bs4 import BeautifulSoup

import pandas as pd
import time

#Open Webpage with Unusual Options
driver = webdriver.Firefox()
driver.get("http://www.optionetics.com/marketdata/MarketRanker.aspx?action=optionranker")
driver.maximize_window()

#select the option of "Highest Volume" from dropdownbox
dropDownID         = "opt_chmain_ddlOptionRankerType"
dropDownElement    = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id(dropDownID))
Select(dropDownElement).select_by_visible_text("HighestVolume")

#Adding delay so that webpage loads
time.sleep(2)

#Start of scraping the webpage
html = driver.page_source
soup = BeautifulSoup(html,"lxml")


#Find first stock symbol
#Find the items on the page and write to CSV File
rows = soup.find_all('tr')[1:]
items_list =[]
for i,row in enumerate(rows):
    if i <50:
        StockInfo=row.findAll('td')
        items = [str(info.text.strip()) for info in StockInfo]
        #print(items)
        items_list.append(items)
#print(items_list)


#Write data to pandas file
df = pd.DataFrame(items_list,columns=('Rank','Stock', 'Option', 'ClosePrice','Change','Volume','VolumeChange','OI', 'OI Change'))
date_today= time.strftime("%d_%m_%Y")
filename = "UOA_"+date_today+".txt"
df.to_csv(filename, sep='\t', index=False)


#close the driver
driver.quit()

