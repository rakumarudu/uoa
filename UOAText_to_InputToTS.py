
# coding: utf-8

# In[93]:

from os import listdir
from os.path import isfile, join
import pandas as pd


# In[159]:

#Parse Raw Website Data and Prepare data to write for Tradestation
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import time

def createTSInputFile(filename_in, verbose =False):
    df = pd.read_csv(filename_in, sep='\t')
    df_TS = pd.DataFrame(index=df.index.copy(), columns = ['Stock'])
    # Copy and write the data in alphabetically sorted order
    #Tradestation expects data in sorted order
    #Also tradestation expects each symbol only once
    symbols = df['Stock'].copy().tolist() 
    symbols.sort() #Tradestation wants data in sorted format
    df_TS['Stock'] = pd.Series(symbols)
    sLength = len(df_TS['Stock'])

    #Append Date Column, get the date from filename
    #example string = "UOA_02_12_2016.csv"
    str1 = filename_in.split(".")[0]
    str2= str1.split("_")
    #reqired date format for TS: %d%m%Y"
    date_TS_str=str2[1]+str2[2]+str(int(str2[3])-1900) #Subtract 1900 from year for Tradestation
    df_TS['Date'] = pd.Series([date_TS_str for i in range(sLength)])
    
    
    #Write data to Tradestation File
    #Write No Pandas Index
    filename_TS = filename_in.split('.')[0]+"_TS.csv"
    with open(filename_TS, 'w') as f:
        df_TS.to_csv(f, header=False, index = False)
    if verbose:
        print("-----------")
        print("Input Filename:", filename_in)
        print("Date:", date_TS_str)
        print("Number of Symbols Detected: ", len(df_TS))
        print("Symbols:", symbols)
        print("Output FileName:", filename_TS)
        print("-----------")
    return filename_TS


# In[161]:

#Read Text File
#Enter path of Directory
directory = '/Users/kalyanramu/Downloads/'
dirs = os.listdir(directory)
files_list=[]

#Get list of text files with .txt extension
for file in dirs:
     if file.endswith(".txt") and file.startswith("UOA"):
        files_list.append(file)

print(files_list)

#Convert Files
for filename in files_list:
    #if os.path.exists(filename):
    createTSInputFile(filename_in = directory+filename, verbose=True)


# In[ ]:



